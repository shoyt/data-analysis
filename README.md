# Data Analysis Overview

## Description

This repository holds personal data analysis projects

## Errors, Criticisms, Comments, Questions

[Contact me](mailto:dev@shoyt.me)

# Projects

## covid_and_voting_trends

### Title: State Voting Trends and COVID-19 Case Count Analysis

The purpose of this analysis is to determine whether there is a correlation between a state's cumualtive COVID-19 cases per capita and the percentage of votes in favor of Donald Trump in the 2016 and 2020 elections.

Write-up of the analysis can be found [here](shoyt.ch/projects/covid_and_voting_trends.html)

### Data Sources

**Election Results**
The election data was obtained from the [Harvard Dataverse and the MIT Election Lab](https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/42MVDX). This data contains U.S Presidential Election results from 1976 - 2020.

**COVID-19 Case Counts**
The COVID-19 cumulative case count data was obtained from the [CDC's data repository](https://data.cdc.gov/Case-Surveillance/United-States-COVID-19-Cases-and-Deaths-by-State-o/9mfq-cb36). This data contains cumulative case counts and death counts for each state over time.

**Population**
The population data used to compute the per capita case counts was obtained from the [US Census 2021 Estimates](https://www.census.gov/newsroom/press-kits/2021/2021-national-state-population-estimates.html). This data contains national and state population estimates for the year 2021.

**2020 Electoral Map**
The 2020 Electoral Map data was manually created based on the official results page at [ABC](https://abcnews.go.com/Elections/2020-us-presidential-election-results-live-map).

